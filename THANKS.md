Thanks
=============================

**Special thanks for these projects who made really much help**

Third Party
-----------------------------

- [`lvgl`]() - Really a powerful graphics library for embedded system

- [`pico-sdk`]() - Raspberry Pi Pico offical SDK

- [`littlefs`](https://github.com/littlefs-project/littlefs) - A high-integrity embedded file system

- [`FreeRTOS kernel`](https://github.com/FreeRTOS/FreeRTOS-Kernel) - A great Real-Time Operating System

Tools Software
-----------------------------

- [`squareline studio`](https://squareline.io/) - lvgl offical ui designer

- [`lceda pro`](https://pro.lceda.cn/) - a pcb eda software based on website by JLC.

- [`autodesk fusion 360`](https://www.autodesk.com/products/fusion-360/overview?term=1-YEAR&tab=subscription) - a powerful 3D model building software by Autodesk

Manufacturers
-----------------------------

- [`JLC`](https://www.jlc.com/) - PCB & PCBA

- [`Good Display`](https://www.good-display.com/) - E-paper display panel